Assignment 6
------------

- textcollage/TextCollage.java (example GUI application)

Compile and run
---------------

- javac textcollage/TextCollage.java (compile)
- java textcollage.TextCollage (run)

Test environment
----------------

- os lubuntu 16.04 kernel version 4.13.0
- javac version 11.0.5
